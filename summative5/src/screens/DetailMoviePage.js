import ItemMovie from "../components/movie/ItemMovie";
import DetailMovie from "../components/movie/DetailMovie";
import { useEffect } from "react";
import { useLocation } from "react-router";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { moviesActions } from "../stores/movie";
import { Row } from "react-bootstrap";
import { API_KEY } from "../constant";

const DetailMoviePage = () => {
    const location = useLocation();
    const detail = useSelector((state) => state.movies.detail);
    const [, , , movieId] = location.pathname.split("/");
    const dispatch = useDispatch();
    console.log(API_KEY)
    useEffect(() => {
        axios
            .get(
                `https://api.themoviedb.org/3/movie/${movieId}?api_key=${API_KEY}&language=en-US`
            )
            .then((res) => {
                dispatch(moviesActions.getDetailMovies(res.data));
            });
    }, [dispatch]);

    return (
        <>
            <Row className="m-3 mb-8">
                <ItemMovie
                    rate={detail.vote_average}
                    id={detail.id}
                    title={detail.title}
                    poster={detail.poster_path}
                    release={detail.release_date}
                />

                <DetailMovie
                    className="mt-5"
                    backdrop_path={detail.backdrop_path}
                    overview={detail.overview}
                    title={detail.title}
                    status={detail.status}
                    tagline={detail.tagline}
                    voteAverage={detail.vote_average}
                    voteCount={detail.vote_count}
                />
            </Row>
        </>
    );
};
export default DetailMoviePage;
