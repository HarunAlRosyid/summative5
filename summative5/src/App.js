import "./App.css";
import { Fragment } from "react";
import { useSelector } from "react-redux";
import MainPage from "./screens/MainPage";
import LoginPage from "./screens/LoginPage";
import RegisterPage from "./screens/RegisterPage";
import NotFoundPage from "./screens/NotFoundPage";
import Footer from "./components/footer/Footer";
import Navigation from "./components/header/Navigation";
import { Routes, Route } from "react-router";
import DetailMoviePage from "./screens/DetailMoviePage";
function App() {
    const isAuth = useSelector((state) => {
        return state.auth.isAuth;
      });
    return (
        <div className="bg font-style">
            <Fragment>
                {isAuth ? (
                    <>
                        <Navigation />
                        <Routes>
                            <Route path="/" element={<MainPage />} />
                            <Route path="*" element={<NotFoundPage />} />
                            <Route path="/movie/detail/:idMovies" element={<DetailMoviePage />} />
                        </Routes>
                        <Footer />
                    </>
                ) : (
                    <Routes>
                        <Route path="/" element={<LoginPage />} />
                        <Route path="/register" element={<RegisterPage />}/>
                        <Route path="*" element={<LoginPage />} />
                    </Routes>
                )}
            </Fragment>
        </div>
    );
}

export default App;
