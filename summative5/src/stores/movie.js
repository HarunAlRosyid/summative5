import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    nowPlaying: [],
    popular: [],
    detail: [],
    video: [],
};

const moviesSlices = createSlice({
    name: "movie",
    initialState: initialState,
    reducers: {
        getAllMoviesNow(state, data) {
            state.nowPlaying = data.payload;
        },
        getAllMoviesPopular(state, data) {
            state.popular = data.payload;
        },
        getDetailMovies(state, data) {
            state.detail = data.payload;
        },
        getTrailerMovies(state, data) {
            state.video = data.payload;
        },
    },
});

export const moviesActions = moviesSlices.actions;

export default moviesSlices.reducer;
