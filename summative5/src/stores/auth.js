import { createSlice } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
const initialState = {
    dataUser: {
        username: "Harun",
        password: "",
    },
    isAuth: false,
};

const authSlices = createSlice({
    name: "auth",
    initialState: initialState,
    reducers: {
        data(state) {
            state.dataUser = { username: "Harun123", password: "" };
        },
        register(state, data) {
            if (data.payload.username === "" || data.payload.password === "") {
                Swal.fire({
                    icon: "error",
                    text: "Username or Password is Empty",
                });
            } else {
                state.isAuth = true;
            }
        },
        login(state, data) {
            if (
                data.payload.username === "harun123" &&
                data.payload.password === "12345678"
            ) {
                state.isAuth = true;
            }  else if (
                data.payload.username === "" &&
                data.payload.password === ""
            ) {
                Swal.fire({
                    icon: "error",
                    text: "Username or Password Empty",
                });
            } else {
                Swal.fire({
                    icon: "error",
                    text: "Username or Password Invalid ",
                });
            }
        },
        logout(state) {
            state.isAuth = false;
        },
    },
});

export const authActions = authSlices.actions;

export default authSlices.reducer;
