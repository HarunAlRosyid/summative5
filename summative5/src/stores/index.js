import { configureStore } from "@reduxjs/toolkit";

import auth from "./auth";
import movies from "./movie"

const store = configureStore({
  reducer: {
    auth: auth,
    movies: movies
  },
});

export default store;
