import { Col } from "react-bootstrap";

export default function NotFound() {
    return (
        <Col
            lg={5}
            className="mt-5 p-5 m-auto shadow-lg rounded-05 l bg-blue text-center "
        >
            <h1 className="fs-200 light">404</h1>
            <h3 className="light">oopss..</h3>
            <p className="light">Page Not Found</p>
        </Col>
    );
}
