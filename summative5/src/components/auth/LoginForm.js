import { Form, Button, Col } from "react-bootstrap";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { authActions } from "../../stores/auth";
import { useNavigate } from "react-router-dom";

export default function LoginForm() {
    const navigate = useNavigate();
    const dispath = useDispatch();
    const [user, setUser] = useState({
        username: "",
        password: "",
    });

    const loginFormHandler = (events) => {
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                [events.target.name]: events.target.value,
            };
        });
    };
    const loginHandler = (events) => {
        events.preventDefault();
        dispath(authActions.login(user));
    };
    function registerLinkHandler() {
        navigate("/register");
    }
    return (
        <Col
            lg={5}
            className="mt-5 mb-6 p-5 m-auto shadow-lg rounded-05 l bg-blue"
        >
            <h5 className="text-center">Sign In to your account</h5>
            <Form className="p-3 d-grid gap-2" onSubmit={loginHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        name="username"
                        type="text"
                        placeholder="Enter username"
                        onChange={loginFormHandler}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        name="password"
                        type="password"
                        placeholder="Enter Password"
                        onChange={loginFormHandler}
                    />
                </Form.Group>
                <Button size="lg" className="bg" type="submit">
                    Sign In
                </Button>
                <hr />
                <p className="text-center">Create new account?</p>
                <Button
                    size="lg"
                    className="btn-primary-customs"
                    onClick={registerLinkHandler}
                >
                    Sign Up
                </Button>
            </Form>
        </Col>
    );
}
