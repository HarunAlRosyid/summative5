import { Form, Button, Col } from "react-bootstrap";
import { useState } from "react";
import { useDispatch,useSelector } from "react-redux";
import { authActions } from "../../stores/auth";
import { useNavigate } from "react-router-dom";

export default function RegisterForm() {
    const isAuth = useSelector((state) => {
        return state.auth.isAuth;
      });
    const navigate = useNavigate();
    function loginLinkHandler() {
        navigate("/");
    }
    const dispath = useDispatch();
    const [user, setUser] = useState({
        username: "",
        password: "",
    });
    const registerFormHandler = (events) => {
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                [events.target.name]: events.target.value,
            };
        });
    };
    const registerHandler = (events) => {
        events.preventDefault();
        dispath(authActions.register(user));
        if(isAuth==false){
            navigate("/register")
        }else if(isAuth==true){
            navigate("/")
        }
    };
    console.log(user);
    return (
        <Col
            lg={5}
            className="mt-5 mb-6 p-5 m-auto shadow-lg rounded-05 l bg-blue"
        >
            <h5 className="text-center">Sign Up to get account</h5>
            <Form className="p-3 d-grid gap-2" onSubmit={registerHandler}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                        name="username"
                        type="text"
                        placeholder="Enter Username"
                        onChange={registerFormHandler}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        name="password"
                        type="password"
                        placeholder="Enter Password"
                        onChange={registerFormHandler}
                    />
                </Form.Group>
                <Button size="lg" className="bg" type="submit">
                    Sign Up
                </Button>
                <hr />
                <p className="text-center">Already have an account?</p>
                <Button
                    size="lg"
                    className="btn-primary-customs"
                    onClick={loginLinkHandler}
                >
                    Sign In
                </Button>
            </Form>
        </Col>
    );
}
