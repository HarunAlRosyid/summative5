import { Modal, Button } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useLocation } from "react-router";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { moviesActions } from "../../stores/movie";
import { API_KEY } from "../../constant";
export default function ModalVideo(props) {
    const dispatch = useDispatch();
    const location = useLocation();
    const [, , , movieId] = location.pathname.split("/");
    const video = useSelector((state) => state.movies.video);
    useEffect(() => {
        axios
            .get(
                `https://api.themoviedb.org/3/movie/${movieId}/videos?api_key=${API_KEY}
                &language=en-US`
            )
            .then((res) => {
                dispatch(
                    moviesActions.getTrailerMovies(res.data.results[0].key)
                );
            });
    }, [dispatch]);

    let videoLink = "https://www.youtube-nocookie.com/embed/" + video;
    console.log(videoLink);

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby=""
            className="bg-trans"
            centered
        >
            <iframe
                height="400px"
                src={videoLink}
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; 
                gyroscope; picture-in-picture"
                allowfullscreen
            ></iframe>
        </Modal>
    );
}
