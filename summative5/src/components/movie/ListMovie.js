import ItemMovie from "./ItemMovie";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { moviesActions } from "../../stores/movie";
import { Row, Tab, Tabs } from "react-bootstrap";
import { API_KEY } from "../../constant";
const ListMovie = () => {
    const nowPlaying = useSelector((state) => state.movies.nowPlaying);
    const popular = useSelector((state) => state.movies.popular);
    const dispatch = useDispatch();
    useEffect(() => {
        async function getMoviesNowAPI() {
            let response = await fetch(
                "https://api.themoviedb.org/3/movie/now_playing?api_key="+API_KEY+
                "&language=en-US&page=1"
            );
            let result = await response.json();
            await dispatch(moviesActions.getAllMoviesNow(result.results));
        }
        try {
            getMoviesNowAPI();
        } catch (err) {
            alert("Render gagal");
        }
        async function getMoviesPopularAPI() {
            let response = await fetch(
                "https://api.themoviedb.org/3/movie/popular?api_key="+API_KEY+
                "&language=en-US&page=1"
            );
            let result = await response.json();
            await dispatch(moviesActions.getAllMoviesPopular(result.results));
        }
        try {
            getMoviesPopularAPI();
        } catch (err) {
            alert("Render gagal");
        }
    }, [dispatch]);
    return (
        <Row className="mt-2 mb-6 p-5">
            <Tabs
                defaultActiveKey="now"
                id="uncontrolled-tab-example"
                className="mb-3"
            >
                <Tab eventKey="now" title="Now Palying">
                    <Row>
                        {nowPlaying.map((value) => {
                            return (
                                <ItemMovie
                                    rate={value.vote_average}
                                    id={value.id}
                                    title={value.title}
                                    poster={value.poster_path}
                                    release={value.release_date}
                                />
                            );
                        })}
                    </Row>
                </Tab>
                <Tab eventKey="popular" title="Popular">
                    <Row>
                        {popular.map((value) => {
                            return (
                                <ItemMovie
                                    rate={value.vote_average}
                                    title={value.title}
                                    poster={value.poster_path}
                                    release={value.release_date}
                                />
                            );
                        })}
                    </Row>
                </Tab>
            </Tabs>
        </Row>
    );
};

export default ListMovie;
