import { Col, Card, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
const ItemMoview = ({ title, poster, release, id,rate }) => {
    const navigate = useNavigate();
    function detailLinkHandler() {
        navigate("/movie/detail/" + id);
    }
    let src = "https://image.tmdb.org/t/p/w500";
    let path = src + poster;
    return (
        <Col lg={2} className="mt-4">
            <Card className="rounded-05 card-links" onClick={detailLinkHandler}>
                <Card.Img className="rounded-05" variant="top" src={path} />
                <Card.Body>
                    <Card.Title>
                        <p>{title}</p>
                        <span className="badge bg-blue float-end rounded-pills">{rate}</span>
                    </Card.Title>
                    <p>{release}</p>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ItemMoview;
