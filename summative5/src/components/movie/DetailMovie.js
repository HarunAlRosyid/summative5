import { Card, Col, Button } from "react-bootstrap";
import play from "../../assets/img/play-button.png";
import { useState } from "react";
import ModalVideo from "./ModalVideo";

export default function DetailMovie({
    backdrop_path,
    overview,
    title,
    status,
    tagline
}) {
    let src = "https://image.tmdb.org/t/p/w500";
    let path = src + backdrop_path;
    const [modalShow, setModalShow] = useState(false);
    return (
        <Col lg={10} className="mt-4 mb-8">
            <Card className="bg-dark text-white bg">
                <Card.Img src={path} alt="Card image" />
                <Card.ImgOverlay>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text>{overview}</Card.Text>
                    <p>Status: {status}</p>
                    <p>Tagline: {tagline}</p>
                    <Button
                        size="lg"
                        className="button-trailer"
                        type="submit"
                        onClick={() => setModalShow(true)}
                    >
                        Trailer
                        {/* href={videoLink}> */}
                        <img
                            src={play}
                            width="30"
                            height="30"
                            className="p-2 d-inline-block align-center"
                            alt="Movies Logo"
                            onClick=""
                        />
                    </Button>
                </Card.ImgOverlay>
            </Card>
            <ModalVideo show={modalShow} onHide={() => setModalShow(false)} />
        </Col>
    );
}
