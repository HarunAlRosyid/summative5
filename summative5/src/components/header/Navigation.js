import logo from "../../assets/img/logo.png";
import { useNavigate, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { authActions } from "../../stores/auth";
import { Nav, Navbar, Container, NavDropdown } from "react-bootstrap";
const Navigation = (payload) => {
    const py = useSelector((payload) => {
        return payload.auth;
    });
    const navigate = useNavigate();
    const dispatch = useDispatch();

    function logoutHandler() {
        navigate("/");
        dispatch(authActions.logout());
    }
    function homeHandler() {
        navigate("/");
    }
    console.log(py);

    return (
        <Navbar
            collapseOnSelect
            expand="lg"
            className="bg-blue"
            variant="dark"
            sticky="top"
        >
            <Container>
                <Navbar.Brand>
                    <img
                        src={logo}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="Movies Logo"
                        onClick={homeHandler}
                    />
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link onClick={homeHandler}>Movie Mania</Nav.Link>
                    </Nav>
                    <Nav>
                        <Nav.Link>Hallo, {py.dataUser.username}</Nav.Link>
                        <Nav.Link onClick={logoutHandler}>Sign Out</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default Navigation;
