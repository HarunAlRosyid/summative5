import { Carousel } from "react-bootstrap";
import venom from "../assets/img/movie-banner/venom.png";
import sang from "../assets/img/movie-banner/sangchi.jpeg";
import spidy from "../assets/img/movie-banner/spiderman.jpg";
export default function Corousel() {
    return (
        <Carousel>
            <Carousel.Item interval={1000} className="">
                <img
                    className="d-block w-100 h-25"
                    src={venom}
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>
                        Nulla vitae elit libero, a pharetra augue mollis
                        interdum.
                    </p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={500}>
                <img
                    className="d-block w-100 h-50"
                    src={sang}
                    alt="Second slide"
                />
                <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100 h-50"
                    src={spidy}
                    alt="Third slide"
                />
                <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>
                        Praesent commodo cursus magna, vel scelerisque nisl
                        consectetur.
                    </p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    );
}
